# CSC 3040: Professionalism Research Paper

## Dependencies

The build requires TeX Live.

You could probably use a different LaTeX processor, but this is what I like to use.

GNU make will make the build process A LOT easier as well.

## Building

Move into the source directory: `cd src`

Build the document: `make`

You will find the finished pdf in the src directory.
Do not commit this pdf.

## Miscellaneous

Most work should be done in the src directory.
I make each section its own tex file and then include it in the paper.tex with the /input processor.
If you add a new section, make a new tex file with a two digit number, a hyphen, then the section's name.
Pad with zero's if the section is still a single digit.
After the file is made, add it to the makefile paper.pdf rule and the document section of paper.tex.

The source names in the bib file and source directory are the lead author's surname and year published.
They are fully lowercase in order to assist command line manipulation.
This is not any recognized standard, but I thought it would work alright.