\section{Intrusion Methods}

\subsection{Voice Commands}

In order to display certain vulnerabilities found in IVA voice commands, Khalid Nuhmood Malik, Hafiz Malik, and Roland Baumann conducted a series of experiments involving both the Amazon Echo and Google Home.
The first of these experiments simply involved asking Alexa to place an online order, using voice commands provided by a non-owner.
Alexa’s voice profiling was unable to identify who was issuing the commands. Following this, a recording of the owner asking, “Alexa, who am I?” was played; Alexa was able to correctly identify the owner, thus showing that prerecorded commands can be given.
The next experiment was conducted with Google Home to display its own voice profiling in two small tests using pre-recorded instances of the Home’s wake word, “OK Google.”
In the first test, the wake word was played back, followed by an online order placed by a completely different voice, and in the second test, the wake word recording was once again played back, followed by a different voice asking Google who was speaking.
In the former test, the transaction was successful, and in the latter, Google identified that the owner was asking who was speaking. These results show that the wake word, “OK Google,” is Google Home’s only means of voice verification. \cite{malik2019}

The third and final experiment was performed using both IVA’s.
An Amazon Echo was hidden behind a TV, and using Alexa’s Drop In Audio Conferencing feature, communicated with another Echo in a different location.
A recorded instance of the Google Home owner issuing a command to turn on an office lamp was played to the hidden Echo, and through the conference was given to the Home, which was then able to successfully turn on the lamp.
This shows that should a user have these IVA’s within proximity to each other, they could be taken advantage of. \cite{malik2019}
While this experiment was only done with a command to turn on a light, much more secure IoT devices can be hooked up to either the Echo or the Home.
For instance, if a person’s home security is dependent on Google Home, and the owner is also in possession of an Amazon Echo, an attacker can play back a recording of the homeowner asking Google to unlock their doors, and succeed in compromising their security.

\subsection{Man-in-the-Middle}

In a study conducted by Richard Mitev, Markus Miettinen, and Ahmad-Reza Sadeghi, a successful man-in-the-middle attack was conducted against an Amazon Echo by manipulating the interface common to all Skills to perform a complete hijacking of how a user interacts with other skills using otherwise benign functions.
They cite a key difference between skills and smartphone apps that was crucial to their ability to place malicious skills into the Amazon Alexa environment.
Skills are "merely third-party-provided service extensions" that can "only react to invocations." \cite{mitev2019}
As such, although they must be tested and certified by Amazon to be listed on the Amazon repository, there is nothing to prevent or detect modifications to these skills post-certification.
These skills are hosted on their own third-party remote servers that Amazon has no jurisdiction over and does not monitor.
Mitev and the other researchers also drew attention to the physical property of microphones that allows for the production of frequencies outside the range of human hearing.
This can be used to inject commands or jam human commands to manipulate an IVA without going through the effort of compromising the device itself.
From these, the researchers produced an attack framework they dubbed Lyexa, consisting of four steps: "Command Jamming, Malicious Skill Invocation, Retrieving benign data from benign Skill, and lastly Echoing modified data to user." \cite{mitev2019}

The framework assumes that there is a compromised IoT device in the home that is within range of the Echo unit and is also equipped with a microphone and speaker.
This requirement is easily met with many variants of IoT cameras.
Using these, the attack will listen for a human invocation to the Echo unit and proceed to jam it with audio signals that are outside the range of human hearing.
While doing so, the compromised IoT device will record the user’s input.
As soon as the input phase has ended, the device will cease blocking and transmit a call to the Echo that invokes the malicious skill rather than the skill that the user intended.
From here, the malicious skill can forward the command to the intended skill.
This allows the malicious skill to read and edit the data flow in both directions, thus completing the man in the middle attack.
All of this is done on-the-fly. It can return any input it likes to the user through Alexa’s voice to emulate the execution of the intended skill.
The researchers tested this successfully on the Nuki Smart Lock to prevent the user from locking a door while returning a message that the door had indeed been locked.
They note, however, that the speaker technology cannot transmit these types of signals a great distance and they were only able to jam from up to 30 centimeters away and they were only able to transmit inaudible commands from 45 centimeters away.

\subsection{Skill Squatting}

Another safety concern for Alexa involves its use of skills, specifically a form of attack known as skill squatting.
Skill squatting is similar to typo squatting, a method through which attackers purchase domain names which derive from another domain address, especially frequently accessed ones such as Google or Wikipedia.
Skill squatting, in turn, refers to the publication of skills with names similar to those of existing skills.
While typo squatting necessitates human error to work, skill squatting is marginally more reliable in that it relies not on error, but on similar phonemes in human language.
One such frequently cited example is the similarity between the word “facts” and “fax.”
Alexa can have a skill focused on “cat facts,” as well as one named “cat fax”; should a user ask Alexa to tell them cat facts, this could potentially confuse it, and cause it to run the cat fax skill instead. \cite{kumar2019}

Deepak Kumar, Riccardo Paccagnella, Paul Murley, Eric Hennenfent, Joshua Mason, Adam Bates, and Michael Bailey from the University of Illinois at Urabana-Champaign focus on skill squatting when discussing potential vulnerabilities in Alexa.
In \cite{kumar2019}, they compare numerous phonemes to show how skill publishers can take advantage of human speech patterns, such as “boil an egg” versus "Boyle an egg," and "comic con dates" versus "comic khan dates." \cite{kumar2019}
A case study is also presented to demonstrate a potential example of phishing that can arise from skill squatting.
Through the American Express skill, users can use the abbreviation “Amex” to ask Alexa to grant them access to their American Express account.
The problem lies in the “Amex” abbreviation; a user with malicious intent could publish a skill for Alexa titled "Am X."
If this skill were to be available, then a user could ask Alexa to sign them into their Amex account, but without a clear means of differentiation, Alexa could activate the Am X skill without the user knowing.
The user would then provide their Amex account login information to the developer of the Am X skill. \cite{kumar2019}
